{pkgs, ...}: let
  pkgs = import <nixpkgs> {};
  kmonad-bin = ./kmonad-darwin-bin; # Local build
  kmonad = pkgs.runCommand "kmonad" {} ''
    #!${pkgs.stdenv.shell}
    mkdir -p $out/bin
    cp ${kmonad-bin} $out/bin/kmonad
    chmod +x $out/bin/*
  '';
in {
  environment = {
    systemPackages = with pkgs; [kmonad];

    etc."kmonad/apple.kbd".text = ''
      (defcfg
        allow-cmd true
        input (iokit-name "Apple Internal Keyboard / Trackpad")
        output (kext)
        fallthrough true
      )

      #|
      (template
        _           _      _     _     _     _     _     _     _     _     _     _    _
                    _      _     _     _     _     _     _     _     _     _     _    _    _
        _           _      _     _     _     _     _     _     _     _     _     _    _
        _           _      _     _     _     _     _     _     _     _     _     _    _    _
        _     _     _      _     _     _     _     _     _     _     _     _     _    _
        _     _     _      _                 _                 _     _     _     _    _
      )
      |#

      (defsrc
        esc         f1     f2    f3    f4    f5    f6    f7    f8    f9    f10   f11  f12
                    1      2     3     4     5     6     7     8     9     0     -    =    bspc
        tab         q      w     e     r     t     y     u     i     o     p     [    ]
        caps        a      s     d     f     g     h     j     k     l     ;     '    \    ret
        lsft  grv   z      x     c     v     b     n     m     ,     .     /     rsft up
        fn    lctl  lalt   lmet              spc               rmet  ralt  left  down rght
      )

      (deflayer default
        _           brdn   brup  lp    mctl  bldn  blup  prev  pp    next  mute  vold volu
                    1      2     3     4     5     6     7     8     9     0     -    =    bspc
        tab         q      @escw @esce r     t     y     u     @bspi @bspo p     [    ]
        @ctle       @meta  @alts @ctld @sftf g     h     @sftj @ctlk l     @met; '    \    ret
        lsft  grv   z      x     c     v     b     n     m     ,     .     /     rsft up
        fn    lctl  lalt   @numt             spc               @navr ralt  left  down rght
      )

      (deflayer nav
        _           _      _     _     _     _     _     _     _     _     _     _    _
                    _      _     _     _     _     _     _     _     _     _     _    _    _
        _           brdn   brup  prev  pp    next  mute  vold  volu  M-c   M-v   _    _
        _           _      _     _     _     _     left  down  up    right ctl   _    _    _
        _     _     _      _     _     _     _     _     _     _     _     _     _    _
        _     _     _      _                 _                 _     _     _     _    _
      )

      (deflayer num
        _           _      _     _     _     _     _     _     _     _     _     _    _
                    _      _     _     _     _     _     _     _     _     _     _    _    _
        _           `      '    \(    \)     _     \     7     8     9     -     _    _
        _           met    alt   ctl   sft   _     _     4     5     6     +     _    _    _
        _     _     {      }     [     ]     _     0     1     2     3     =     _    _
        _     _     _      _                 _                 _     _     _     _    _
      )

      (deflayer escw
        _           _      _     _     _     _     _     _     _     _     _     _    _
                    _      _     _     _     _     _     _     _     _     _     _    _    _
        _           _      _     esc   _     _     _     _     _     _     _     _    _
        _           _      _     _     _     _     _     _     _     _     _     _    _    _
        _     _     _      _     _     _     _     _     _     _     _     _     _    _
        _     _     _      _                 _                 _     _     _     _    _
      )

      (deflayer esce
        _           _      _     _     _     _     _     _     _     _     _     _    _
                    _      _     _     _     _     _     _     _     _     _     _    _    _
        _           _      esc   _     _     _     _     _     _     _     _     _    _
        _           _      _     _     _     _     _     _     _     _     _     _    _    _
        _     _     _      _     _     _     _     _     _     _     _     _     _    _
        _     _     _      _                 _                 _     _     _     _    _
      )

      (deflayer bspi
        _           _      _     _     _     _     _     _     _     _     _     _    _
                    _      _     _     _     _     _     _     _     _     _     _    _    _
        _           _      _     _     _     _     _     _     _     bspc  _     _    _
        _           _      _     _     _     _     _     _     _     _     _     _    _    _
        _     _     _      _     _     _     _     _     _     _     _     _     _    _
        _     _     _      _                 _                 _     _     _     _    _
      )

      (deflayer bspo
        _           _      _     _     _     _     _     _     _     _     _     _    _
                    _      _     _     _     _     _     _     _     _     _     _    _    _
        _           _      _     _     _     _     _     _     bspc  _     _     _    _
        _           _      _     _     _     _     _     _     _     _     _     _    _    _
        _     _     _      _     _     _     _     _     _     _     _     _     _    _
        _     _     _      _                 _                 _     _     _     _    _
      )

      (defalias  ;; Home row mods
        meta (tap-hold-next-release 200 a met)
        alts (tap-hold-next-release 200 s alt)
        ctld (tap-hold-next-release 200 d ctl)
        sftf (tap-hold-next-release 200 f sft :timeout-button f)

        sftj (tap-hold-next 200 j sft :timeout-button j)
        ctlk (tap-hold-next 200 k ctl :timeout-button k)
        met; (tap-hold-next 200 ; met)
      )

      (defalias  ;; combos
        escw (tap-hold-next 20 w (layer-toggle escw) :timeout-button w)
        esce (tap-hold-next 20 e (layer-toggle esce) :timeout-button e)

        bspi (tap-hold-next 20 i (layer-toggle bspi) :timeout-button i)
        bspo (tap-hold-next 20 o (layer-toggle bspo) :timeout-button o)
      )

      (defalias  ;; layers
        ctle (tap-next-release esc ctl)

        numt (tap-hold-next-release 200 tab (layer-toggle num))
        navr (tap-hold-next-release 200 ret (layer-toggle nav))
      )
    '';

    etc."kmonad/gk61.kbd".text = ''
      (defcfg
        input (iokit-name "TMKB GK61")
        output (kext)
        fallthrough true
        allow-cmd true
      )

      #|
      (template
        _     _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _                 _                 _     _     _
      )
      |#

      (defsrc
        esc   1     2     3     4     5     6     7     8     9     0     -     =     bspc
        tab   q     w     e     r     t     y     u     i     o     p     [     ]     \
        caps  a     s     d     f     g     h     j     k     l     ;     '     ret
        lsft  z     x     c     v     b     n     m     ,     .     /     rsft
        lctl  lmet  lalt              spc               ralt  cmp   rctl
      )

      (deflayer default
        q     @escw @esce r     t     XX    XX    XX    y     u     @bspi @bspo p     XX
        @meta @alts @ctld @sftf @gamg XX    XX    XX    h     @sftj @ctlk @altl @met; '
        z     x     c     @micv b     XX    XX    XX    n     m     ,     .     /
        XX    XX    @numt spc   XX    XX    XX    ret   @navd XX    XX    XX
        XX    XX    XX                XX                XX    XX    XX
      )

      (deflayer game
        esc   1     2     3     4     5     6     7     8     9     0     -     =     bspc
        tab   q     w     e     r     t     y     u     i     o     p     [     ]     \
        caps  a     s     d     f     @defg h     j     k     l     ;     '     ret
        lsft  z     x     c     v     b     n     m     ,     .     /     rsft
        lctl  lmet  lalt              spc               ralt  cmp   rctl
      )

      (deflayer num
        `     '     \(    \)    _     _     _     _     \     7     8     9     -     _
        met   _     ctl   sft   _     _     _     _     €     4     5     6     +     _
        {     }     [     ]     _     _     _     _     0     1     2     3     =
        _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _                 _                 _     _     _
      )

      (deflayer nav
        M-1   M-2   M-3   M-4   M-5   _     _     _     mute  vold  volu  M-c   M-v   _
        met   alt   ctl   sft   _     _     _     _     left  down  up    right ctl   _
        M-6   M-7   M-8   M-9   M-0   _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _                 _                 _     _     _
      )

      (deflayer escw
        _     _     esc   _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _                 _                 _     _     _
      )

      (deflayer esce
        _     esc   _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _                 _                 _     _     _
      )

      (deflayer bspi
        _     _     _     _     _     _     _     _     _     _     _     bspc  _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _                 _                 _     _     _
      )

      (deflayer bspo
        _     _     _     _     _     _     _     _     _     _     bspc  _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _                 _                 _     _     _
      )

      (deflayer acentos
        _     _     @é    _     _     _     _     _     _     @ú   @í     @ó    _     _
        @á    _     _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _     _     _     _     _     _     @ñ    _     _     _     _
        _     _     _     _     _     _     _     _     _     _     _     _
        _     _     _                 _                 _     _     _
      )
      (defalias  ;; acentos
        á (tap-macro A-e a)
        é (tap-macro A-e e)
        í (tap-macro A-e i)
        ó (tap-macro A-e o)
        ú (tap-macro A-e u)
        ñ (tap-macro A-n n)
      )

      (defalias  ;; Home row mods
        meta (tap-hold-next 200 a met :timeout-button a)
        alts (tap-hold-next-release 200 s alt)
        ctld (tap-hold-next 200 d ctl :timeout-button d)
        sftf (tap-hold-next 200 f sft :timeout-button f)

        sftj (tap-hold-next 200 j sft :timeout-button j)
        altl (tap-hold-next-release 200 l (layer-toggle acentos))
        ctlk (tap-hold-next 200 k ctl :timeout-button k)
        met; (tap-hold-next 200 ; met)
      )

      (defalias  ;; combos
        escw (tap-hold-next 20 w (layer-toggle escw) :timeout-button w)
        esce (tap-hold-next 20 e (layer-toggle esce) :timeout-button e)

        bspi (tap-hold-next 20 i (layer-toggle bspi) :timeout-button i)
        bspo (tap-hold-next 20 o (layer-toggle bspo) :timeout-button o)
      )

      (defalias  ;; layers
        game (layer-switch game)
        gamg (tap-hold-next 3000 g @game)
        def (layer-switch default)
        defg (tap-hold-next 3000 g @def)

        nav (layer-toggle nav)
        navd (tap-next-release del @nav)

        num (layer-toggle num)
        numt (tap-hold-next-release 200 tab (layer-toggle num))
      )

      (defalias  ;; commands
        mic (cmd-button "osascript -e 'set volume input volume 80'"
                        "osascript -e 'set volume input volume 0'")
        micv (tap-hold 200 v @mic)
      )
    '';
  };
}
# To test run: `sudo launchctl load /Library/LaunchDaemons/local.kmonad-apple.plist`
/*
Copy the following text to /Library/LaunchDaemons/local.kmonad-apple.plist

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
    <key>Label</key>
    <string>local.kmonad-apple</string>
    <key>Program</key>
    <string>/run/current-system/sw/bin/kmonad</string>
    <key>ProgramArguments</key>
    <array>
      <string>/run/current-system/sw/bin/kmonad</string>
      <string>/etc/kmonad/apple.kbd</string>
    </array>
    <key>RunAtLoad</key>
    <true />
    <key>StandardOutPath</key>
    <string>/tmp/kmonad.stdout</string>
    <key>StandardErrorPath</key>
    <string>/tmp/kmonad.stderr</string>
  </dict>
</plist>
*/
/*
Copy the following text to /Library/LaunchDaemons/local.kmonad-gk61.plist

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
    <key>Label</key>
    <string>local.kmonad-gk61</string>
    <key>Program</key>
    <string>/run/current-system/sw/bin/kmonad</string>
    <key>ProgramArguments</key>
    <array>
      <string>/run/current-system/sw/bin/kmonad</string>
      <string>/etc/kmonad/gk61.kbd</string>
    </array>
    <key>RunAtLoad</key>
    <true />
    <key>StandardOutPath</key>
    <string>/tmp/kmonad.stdout</string>
    <key>StandardErrorPath</key>
    <string>/tmp/kmonad.stderr</string>
  </dict>
</plist>
*/
