{
  config,
  pkgs,
  lib,
  ...
}: let
  myUser = "pbl";

  pkgs = import <nixpkgs> {};

  kmonad-bin = ./kmonad-bin; # Local build
  # kmonad-bin = pkgs.fetchurl {
  #   url = "https://github.com/kmonad/kmonad/releases/download/0.4.1/kmonad-0.4.1-linux";
  #   sha256 = "839e58e7cc23d6dd06846f343c0c9be0f927698189e497da35ef5196703f7a8f";
  #
  #   url = "https://github.com/kmonad/kmonad/releases/download/0.4.0/kmonad-0.4.0-linux";
  #   sha256 = "b6dd5e5192a4f0a1b84387def3b145ca136472df070b9fb3ef26f4e3b9300267";
  # };
  kmonad = pkgs.runCommand "kmonad" {} ''
    #!${pkgs.stdenv.shell}
    mkdir -p $out/bin
    cp ${kmonad-bin} $out/bin/kmonad
    chmod +x $out/bin/*
  '';

  configFile-infinity = pkgs.writeText "kmonad-infinity" ''
    (defcfg
      input (device-file "/dev/input/by-path/platform-i8042-serio-0-event-kbd")
      output (uinput-sink "kmonad-infinity")
      fallthrough true
      allow-cmd true
    )

    #|
    (template
      XX          XX    XX    XX    XX    XX   XX     XX    XX    XX    XX    XX    XX    XX    XX    XX   XX

      _           _     _     _     _     _    _      _     _     _     _     _     _     _     _     _    _
      _           _     _     _     _     _    _      _     _     _     _     _     _     _
      _           _     _     _     _     _    _      _     _     _     _     _     _     _
      _           _     _     _     _     _    _      _     _     _     _     _     _
      _     _     _     _     _     _     _     _     _     _     _     _     _
      _           _     _           _                       _     _     _     _     _
                                                                        _     _     _
    )
    |#

    (defsrc
      esc         f1    f2    f3    f4    f5   f6     f7    f8    f9    f10   f11   f12   prnt  pause ins    del
      grv         1     2     3     4     5    6      7     8     9     0     -     =     bspc
      tab         q     w     e     r     t    y      u     i     o     p     [     ]     ret
      caps        a     s     d     f     g    h      j     k     l     ;     '     \
      lsft  102d  z     x     c     v     b    n      m     ,     .     /     rsft
      lctl        lmet  lalt        spc                     ralt  rctl  pgup  up    pgdn
                                                                        left  down  rght
      )

    (deflayer base
      XX          _     _     _     _     _    _      _     _     _     _     _     _     _     _     _    _
      _           _     _     _     _     _    _      _     _     _     _     _     _     _
      @tab        _     @escw @esce _     _    _      _     @bspi @bspo _     _     _     _
      @cesc       @meta @alts @ctld @sftf _    _      @sftj @ctlk @altl @met; _     _
      _     _     _     _     _     _     _    _     _     _     _     @nav/ _
      _           _     @numt       _          @nret  bspc        _     _     _
                                                                  _     _     _
    )

    (deflayer nav
      XX          XX    XX    XX    XX    XX   XX     XX    XX    XX    XX    XX    XX    XX    XX    XX   XX
      _           _     _     _     _     _    _      _     _     _     _     _     _     _
      XX          brdn  brup  @e    XX    XX   XX     @u    @i    @o    XX    XX    XX    XX
      _           @a    |     {     }     home left   down  up    rght  end   _     _
      XX    XX    vold  volu  mute  XX    XX   @n     XX    XX    @cpy  @pst  XX
      _           _     _           _          _      _           _     _     _
                                                                  _     _     _
    )

    (deflayer num
      XX          XX    XX    XX    XX    XX   XX     XX    XX    XX    XX    XX    XX    XX    XX    XX   XX
      _           _     _     _     _     _    _      _     _     _     _    _     _     _
      _           `     '    \(    \)     _    \      7     8     9     -    _     _     _
      _           met   alt   ctl   sft   _    _      4     5     6     +    _     _
      _     {     }     [     ]     _     _    0      1     2     3    =     _
      _           _     _           _          _      _           _     _    _
                                                                  _     _    _
    )

    (deflayer escw
      XX          XX    XX    XX    XX    XX   XX     XX    XX    XX    XX    XX    XX    XX    XX    XX   XX
      _     _     _     _     _     _     _      _     _     _     _    _     _     _
      _     _     _     esc   _     _     _      _     _     _     _    _     _     _
      _     _     _     _     _     _     _      _     _     _     _    _     _
      _     _     _     _     _     _     _      _     _     _     _    _     _
      _     _     _           _                        _     _     _    _     _
                                                                   _    _     _
    )

    (deflayer esce
      XX          XX    XX    XX    XX    XX   XX     XX    XX    XX    XX    XX    XX    XX    XX    XX   XX
      _     _     _     _     _     _     _      _     _     _     _    _     _     _
      _     _     esc   _     _     _     _      _     _     _     _    _     _     _
      _     _     _     _     _     _     _      _     _     _     _    _     _
      _     _     _     _     _     _     _      _     _     _     _    _     _
      _     _     _           _                        _     _     _    _     _
                                                                   _    _     _
    )

    (deflayer bspi
      XX          XX    XX    XX    XX    XX   XX     XX    XX    XX    XX    XX    XX    XX    XX    XX   XX
      _     _     _     _     _     _    _      _     _    _    _    _     _     _
      _     _     _     _     _     _    _      _     _    bspc _    _     _     _
      _     _     _     _     _     _    _      _     _    _    _    _     _
      _     _     _     _     _     _    _      _     _    _    _    _     _
      _     _     _           _          _      _          _    _    _
                                                           _    _    _
    )

    (deflayer bspo
      XX          XX    XX    XX    XX    XX   XX     XX    XX    XX    XX    XX    XX    XX    XX    XX   XX
      _     _     _     _     _     _    _      _     _    _    _    _     _     _
      _     _     _     _     _     _    _      _     bspc _    _    _     _     _
      _     _     _     _     _     _    _      _     _    _    _    _     _
      _     _     _     _     _     _    _      _     _    _    _    _     _
      _     _     _           _          _      _          _    _    _
                                                           _    _    _
    )

    (defalias  ;; layers
      nav (layer-toggle nav)
      nav/ (tap-next-release / @nav)
      cesc (tap-next-release esc ctl)
      salt (tap-next-release ; ralt)
      tab (tap-next-release tab @nav)
      nret (tap-hold-next 200 ret @nav)

      numt (tap-hold-next-release 200 tab (layer-toggle num))
    )

    ;; (defalias
    ;;   bth (cmd-button "pactl set-source-mute @DEFAULT_SOURCE@ 0"
    ;;                   "pactl set-source-mute @DEFAULT_SOURCE@ 1")
    ;;   mic (tap-hold 200 \\ @bth)
    ;; )

    (defalias ;; home row mod
      meta (tap-hold-next-release 200 a met :timeout-button a)
      alts (tap-hold-next-release 200 s alt)
      ctld (tap-hold-next-release 200 d ctl)
      sftf (tap-hold-next-release 200 f sft)

      sftj (tap-hold-next-release 200 j sft :timeout-button j)
      ctlk (tap-hold-next 200 k ctl :timeout-button k)
      altl (tap-hold-next 200 l ralt :timeout-button l)
      met; (tap-hold-next-release 200 ; met)
    )

    (defalias  ;; combos
      escw (tap-hold-next 20 w (layer-toggle escw) :timeout-button w)
      esce (tap-hold-next 20 e (layer-toggle esce) :timeout-button e)

      bspi (tap-hold-next 20 i (layer-toggle bspi) :timeout-button i)
      bspo (tap-hold-next 20 o (layer-toggle bspo) :timeout-button o)
    )

    (defalias  ;; commands
      ;; cpy (cmd-button "/etc/profiles/per-user/pbl/bin/wl-copy")
      cpy C-S-c
      ;; pst (cmd-button "/etc/profiles/per-user/pbl/bin/wl-paste")
      pst C-S-v

    )
    (defalias  ;; acute
      a (around ralt a)
      e (around ralt e)
      i (around ralt i)
      o (around ralt o)
      u (around ralt u)
      n (around ralt n)
    )
  '';

  configFile-gk61 = pkgs.writeText "kmonad-gk61" ''
    (defcfg
      input (device-file "/dev/gk61")
      output (uinput-sink "kmonad-gk61")
      fallthrough true
      allow-cmd true
    )

    #|
    (template
      _     _     _     _     _     _      _     _    _     _     _    _     _     _
      _     _     _     _     _     _      _     _    _     _     _    _     _     _
      _     _     _     _     _     _      _     _    _     _     _    _     _
      _     _     _     _     _     _      _     _    _     _     _    _
      _     _     _                 _                 _     _     _
    )
    |#

    (defsrc
      esc   1     2     3     4     5      6     7    8     9    0     -     =     bspc
      tab   q     w     e     r     t      y     u    i     o    p     [     ]     \
      caps  a     s     d     f     g      h     j    k     l    ;     '     ret
      lsft  z     x     c     v     b      n     m    ,     .    /     rsft
      lctl  lmet  lalt              spc               ralt  cmp  rctl
    )

    (deflayer default
      q     w     e     r     t     XX    XX    XX   y     u     i     o     p     XX
      @meta @alts @ctld @sftf g     XX    XX    XX   h     @sftj @ctlk @altl @met; XX
      z     x     c     v     b     XX    XX    XX   n     m     ,     .     /
      XX    esc   @numb spc   XX    XX    XX    ret  @navt del   XX    XX
      XX    XX    XX                XX               XX    XX    XX
    )

    (deflayer nav
      _     _     _     _     _    _      _     _    mute  vold  volu  _     _     _
      _     _     _     _     _    _      _     _    left  down  up    right ctl   _
      _     _     _     _     _    _      _     _    _     _     _     _     _
      _     _     _     _     _    _      _     _    _     _     _     _
      _     _     _                _                 _     _     _
    )

    (deflayer num
      `     '     \(    \)    _    _      _     _    _     7     8     9     -     _
      _     _     _     sft   _    _      _     _    _     4     5     6     =     _
      {     }     [     ]     _    _      _     _    0     1     2     3     _
      _     _     _     _     _    _      _     _    _     _     _     _
      _     _     _                _                 _     _     _
    )

    (defalias  ;;layers
      nav (layer-toggle nav)
      navt (tap-next-release tab @nav)

      num (layer-toggle num)
      numb (tap-hold-next-release 200 bspc @num :timeout-button bspc)
    )

    (defalias  ;;home-row mods
      meta (tap-hold 200 a met)
      alts (tap-hold 200 s alt)
      ctld (tap-hold 200 d ctl)
      sftf (tap-hold 200 f sft)

      sftj (tap-hold-next-release 200 j sft :timeout-button j)
      ctlk (tap-hold-next-release 200 k ctl :timeout-button k)
      altl (tap-hold-next-release 200 l alt :timeout-button l)
      met; (tap-hold-next-release 200 ; met :timeout-button ;)
    )

  '';
in {
  environment.systemPackages = with pkgs; [kmonad];
  users.groups = {uinput = {};};

  users.extraUsers.${myUser} = {extraGroups = ["input" "uinput"];};
  services.udev.extraRules = ''
    # KMonad user access to /dev/uinput
    KERNEL=="uinput", MODE="0660", GROUP="uinput", OPTIONS+="static_node=uinput"

    # GK61
    ## $ udevadm info -a /dev/input/event18
    ATTRS{name}=="TMKB GK61", SYMLINK+="gk61"
  '';

  systemd.services."kmonad-infinity" = {
    enable = true;
    description = "kmonad-infinity";
    wantedBy = ["default.target"];
    # environment = {
    #   XDG_RUNTIME_DIR = "/run/user/1000";
    #   WAYLAND_DISPLAY = "wayland-1";
    # };
    serviceConfig.ExecStart =
      "/run/current-system/sw/bin/kmonad "
      + configFile-infinity;
  };
  systemd.services."kmonad-gk61" = {
    enable = true;
    description = "kmonad-gk61";
    wantedBy = ["default.target"];
    serviceConfig = {
      After = "gk61.device";
      ExecStart = "/run/current-system/sw/bin/kmonad " + configFile-gk61;
      Restart = "always";
      RestartSec = "3";
    };
  };
}
