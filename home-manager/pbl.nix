{
  config,
  pkgs,
  lib,
  ...
}: {
  imports = [./apps/devops.nix ./apps/common.nix ./apps/zellij.nix];
  # list of programs
  # https://mipmip.github.io/home-manager-option-search
  home = {
    # packages = with pkgs; [
    #   jq
    #   htop
    # ];
    stateVersion = "23.05";
  };
  programs = {
    home-manager.enable = true;
  };
}
