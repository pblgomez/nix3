{
  pkgs,
  config,
  ...
}: {
  home = {
    packages = with pkgs; [
      kitty
      joplin
      just
      tldr
    ];
  };

  programs = {
    home-manager.enable = true;
    bat = {
      enable = true;
      config = {theme = "DarkNeon";};
    };
    direnv = {
      enable = true;
      enableZshIntegration = true;
      nix-direnv.enable = true;
    };
    fzf = {
      enable = true;
      enableZshIntegration = true;
    };
    starship = {
      enable = true;
      enableZshIntegration = true;
      settings = {
        format = "$all$custom$git_branch$git_status$line_break$battery$character";
        battery = {
          disabled = false;
          display = [
            {
              discharging_symbol = " 󰂎 ";
              style = "bold red";
              threshold = 20;
            }
            {
              discharging_symbol = "💦 ";
              style = "bold yellow";
              threshold = 30;
            }
          ];
        };
        custom.gitlab = {
          command = "git remote -v | grep gitlab";
          require_repo = true;
          symbol = " ";
          format = "$symbol";
          when = "git remote -v | grep gitlab";
        };
        custom.github = {
          command = "git remote -v | grep github";
          require_repo = true;
          symbol = " ";
          format = "$symbol";
          when = "git remote -v | grep github";
        };
        kubernetes = {disabled = false;};
        nix_shell = {disabled = false;};
      };
    };

    zathura = {
      enable = true;
      options = {
        default-bg = "#1a1b26";
        default-fg = "#9aa5ce";
        recolor = "true";
        recolor-darkcolor = "#9aa5ce";
        recolor-lightcolor = "#414868";
      };
    };
    zsh = {
      enable = true;
      dotDir = ".config/zsh";

      enableAutosuggestions = true;
      enableCompletion = true;
      enableSyntaxHighlighting = true;
      history.path = "${config.xdg.dataHome}/zsh/history";
      shellAliases = {
        cat = "bat";

        g = "git";
        gde = "git checkout $(git symbolic-ref refs/remotes/origin/HEAD | awk -F 'origin/' '{print $2}')";
        gitcleanup = "git branch | grep -v $(git symbolic-ref refs/remotes/origin/HEAD | awk -F 'origin/' '{print $2}') | xargs git branch -D && git remote prune origin";

        k = "kubecolor";
        kg = "k get";
        kd = "k describe";
        kk = "cat $(switch | awk -F ' |,' '{print $2}') > ~/.kube/config";
        kns = "switch ns";

        l = "exa --git";
        ll = "l -l";
        la = "l -la";

        update = "sudo nixos-rebuild switch --flake .# --impure";

        v = "nvim";
      };
      shellGlobalAliases = {
        G = "| grep";
        neat = "-o yaml | kubectl neat";
      };
      zplug = {
        enable = true;
        zplugHome = "${config.xdg.dataHome}/zplug";
        plugins = [
          {name = "skywind3000/z.lua";}
          {
            name = "plugins/git-auto-fetch";
            tags = ["from:oh-my-zsh"];
          }
        ];
      };
      initExtra = ''
        command -v Hyprland >/dev/null && [ "$(tty)" = "/dev/tty1" ] && [ -z "$DISPLAY" ] && exec Hyprland

        setopt nocaseglob                                                                     # Ignore case
        zstyle -e ':completion:*' special-dirs '[[ $PREFIX = (../)#(|.|..) ]] && reply=(..)'  # Complete special dir not always ../
        export PATH="$HOME/.local/bin:$PATH"

        zstyle ':completion:*' menu select

        command -v kubecolor >/dev/null 2>&1 && alias kubectl="kubecolor" && compdef kubecolor=kubectl
        alias k='kubecolor'

        export PATH="$PATH:$HOME/.krew/bin"

        command -v direnv >/dev/null && eval "$(direnv hook zsh)"

        # Zellij
        command -v zellij >/dev/null && export ZELLIJ_AUTO_ATTACH="true"
        if [ -n "$DISPLAY" ] && [ -z "$ZELLIJ" ]; then
          if [[ "$ZELLIJ_AUTO_ATTACH" == "true" ]]; then
            zellij attach -c
          else
            zellij
          fi
          if [[ "$ZELLIJ_AUTO_EXIT" == "true" ]]; then
            exit
          fi
        fi
      '';
    };
  };
}
