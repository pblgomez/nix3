{
  unstable,
  pkgs,
  ...
}: {
  home = {
    packages = with pkgs; [
      awscli2
      dogdns
      eksctl
      glab

      kubectl
      kubecolor
      kubernetes-helm
      krew

      stern
      sops
      terraform
      vault
    ];
  };

  programs = {};
}
