let
  pbl-infinity = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEoe8CHQx53FwXs7SxKoYb8qgivXEza6vdIobOOwsYtP";
  pbl-mbp183 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPxvcE3SNJQLy6DkQpRhOCviOh9d9G9Cy0GUsMlo7bh1";
  pbl-nixos3 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMeowHKBy0LDrg+cyMHa8M2J5u8VcmGDERuflsmwuhhN";
  users = [pbl-infinity pbl-mbp183 pbl-nixos3];

  infinity = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEuk3UC+VA1tKsElZQcFweFyaY/tyGZVyYEdeEZ2Hbsk";
  mbp183 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILBtcqizzJxHLRhDDnRS0Kv/5wFELM6DfAjtd1A1Re4h";
  nixos3 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKNfsh+11G5ku0Ork9cR5rZqlCR8F2iWWS1DOOPmF+to";
  systems = [infinity mbp183 nixos3];
in {
  "secrets/awsCreds.age".publicKeys = users ++ systems;
  "secrets/sshConfig.age".publicKeys = users ++ systems;
  "secrets/bookmarks.age".publicKeys = users ++ systems;
  "secrets/temp.age".publicKeys = users ++ systems;
}
