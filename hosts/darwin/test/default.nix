{
  config,
  pkgs,
  unstablePkgs,
  inputs,
  ...
}: {
  environment.systemPackages =
    import ./../../common/common-packages.nix
    {
      inherit pkgs;
      inherit unstablePkgs;
    };
}
