{
  pkgs,
  unstablePkgs,
  lib,
  inputs,
  ...
}: let
  inherit (inputs) nixpkgs nixpkgs-unstable;
in {
  imports = [../../kmonad/kmonad.nix];
  age.secrets = {
    awsCreds = {
      file = ../../secrets/awsCreds.age;
      path = "/home/pbl/.config/aws/credentials";
      mode = "600";
      owner = "pbl";
      group = "users";
    };
    bookmarks = {
      file = ../../secrets/bookmarks.age;
      path = "/home/pbl/.local/share/bookmarks";
      mode = "600";
      owner = "pbl";
      group = "users";
    };
    sshConfig = {
      file = ../../secrets/sshConfig.age;
      path = "/home/pbl/.ssh/config";
      mode = "600";
      owner = "pbl";
      group = "users";
    };
  };
  time.timeZone = "Atlantic/Canary";
  nix = {
    settings = {
      experimental-features = ["nix-command" "flakes"];
      warn-dirty = false;
    };
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 7d";
    };
  };
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    # intel-gpu-tools
    # libva-utils
    # intel-media-driver
    # jellyfin-ffmpeg
  ];
  programs.hyprland.enable = true;

  ## pins to stable as unstable updates very often
  # nix.registry.nixpkgs.flake = inputs.nixpkgs;
  # nix.registry = {
  #   n.to = {
  #     type = "path";
  #     path = inputs.nixpkgs;
  #   };
  #   u.to = {
  #     type = "path";
  #     path = inputs.nixpkgs-unstable;
  #   };
  # };
}
