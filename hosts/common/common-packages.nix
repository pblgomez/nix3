{
  pkgs,
  unstablePkgs,
  ...
}:
with pkgs; [
  ## unstable
  unstablePkgs.yt-dlp

  ## stable
  bitwarden-cli
  lua
  jq
]
