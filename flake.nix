{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-23.05-darwin";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    nixpkgs-darwin.url = "github:NixOS/nixpkgs/nixpkgs-23.05-darwin";

    kmonad = {
      url = "git+https://github.com/kmonad/kmonad?submodules=1&dir=nix";
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager/release-23.05";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-darwin = {
      url = "github:lnl7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = {
    agenix,
    home-manager,
    nixpkgs,
    kmonad,
    nixpkgs-unstable,
    nixpkgs-darwin,
    nix-darwin,
    self,
  } @ inputs: let
    inputs = {inherit nix-darwin home-manager nixpkgs nixpkgs-unstable;};
    # creates correct package sets for specified arch
    genPkgs = system:
      import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
    genDarwinPkgs = system:
      import nixpkgs-darwin {
        inherit system;
        config.allowUnfree = true;
      };
    # creates a nixos system config
    nixosSystem = system: hostName: username: let
      pkgs = genPkgs system;
    in
      nixpkgs.lib.nixosSystem
      {
        inherit system;
        modules = [
          # adds unstable to be available in top-level evals (like in common-packages)
          {_module.args = {unstablePkgs = inputs.nixpkgs-unstable.legacyPackages.${system};};}

          agenix.nixosModules.default
          ./hosts/nixos/${hostName} # ip address, host specific stuff
          home-manager.nixosModules.home-manager
          {
            networking.hostName = hostName;
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              users.${username} = {imports = [./home-manager/${username}.nix];};
            };
          }
          ./hosts/common/nixos-common.nix
          ./hosts/common/common-configs.nix
        ];
      };

    # creates a macos system config
    darwinSystem = system: hostName: username: let
      pkgs = genDarwinPkgs system;
    in
      nix-darwin.lib.darwinSystem
      {
        inherit system inputs;
        modules = [
          # adds unstable to be available in top-level evals (like in common-packages)
          {_module.args = {unstablePkgs = inputs.nixpkgs-unstable.legacyPackages.${system};};}

          agenix.nixosModules.default
          ./hosts/darwin/${hostName} # ip address, host specific stuff
          home-manager.darwinModules.home-manager
          {
            networking.hostName = hostName;
            home-manager = {
              useGlobalPkgs = true;
              useUserPackages = true;
              users.${username} = {imports = [./home-manager/${username}.nix];};
            };
          }
          ./hosts/common/darwin-common.nix
          ./hosts/common/common-configs.nix
        ];
      };
  in {
    darwinConfigurations = {
      test = darwinSystem "aarch64-darwin" "test" "pbl";
      slartibartfast = darwinSystem "aarch64-darwin" "slartibartfast" "alex";
      cat-laptop = darwinSystem "aarch64-darwin" "cat-laptop" "alex";
    };

    nixosConfigurations = {
      testnix = nixosSystem "x86_64-linux" "testnix" "alex";
      nixos3 = nixosSystem "x86_64-linux" "nixos3" "pbl";
    };
    formatter.aarch64-darwin = nixpkgs.legacyPackages.aarch64-darwin.alejandra;
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.alejandra;
  };
}
